import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/app';
import Firebase from './components/firebase'

ReactDOM.render(
  <React.StrictMode>
    <App firebase={new Firebase()} />
  </React.StrictMode>,
  document.getElementById('root')
);

