import React from 'react';
import Account from '../account';
import Home from '../home';

import './app.css'


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user: null
        }
    }

    componentDidMount() {
        this.props.firebase.getAuth().onAuthStateChanged((user) => {
            this.setState({
                user: user
            })
        })
    }

    render() {
        return (
            <div className='container'>
                {this.state.user ? "" : "Please log in to make a list"}
                {this.state.user ? <Account firebase={this.props.firebase}/> : <Home firebase={this.props.firebase} />}
            </div>
        )
    }

}

export default App;

