import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';


var firebaseConfig = {
    apiKey: "AIzaSyDp2Ctti_qpkrze4jUgpEGiusMml0nbTTo",
    authDomain: "todolist-935c1.firebaseapp.com",
    databaseURL: "https://todolist-935c1.firebaseio.com",
    projectId: "todolist-935c1",
    storageBucket: "todolist-935c1.appspot.com",
    messagingSenderId: "28353393818",
    appId: "1:28353393818:web:801393b581d8a89a621b23",
    measurementId: "G-ZS3SG5G3EH"
};

class Firebase {
    constructor() {
        app.initializeApp(firebaseConfig);
        this.auth = app.auth();
        this.db = app.firestore();
    }

    getAuth() {
        return this.auth;
    }

    getItemCollection() {
        return this.db
            .collection("users")
            .doc(this.auth.currentUser.uid)
            .collection("items");

    }

    signInWithGoogle() {
        let provider = new app.auth.GoogleAuthProvider();
        this.auth.signInWithPopup(provider);
    }

    signOut() {
        this.auth.signOut();
    }
}

export default Firebase;
