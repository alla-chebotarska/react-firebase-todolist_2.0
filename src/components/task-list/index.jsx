import React, { Component } from 'react';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CloseIcon from '@material-ui/icons/Close';

import './task-list.css';

export default class TaskList extends Component {

    constructor(props) {
        super(props);
    }

    onDoneClick = (item) => {
        item.data.done = !item.data.done;
        this.props.firebase
            .getItemCollection()
            .doc(item.id)
            .set(
                item.data
            );
    }

    onDeleteClick = (item) => {
        this.props.firebase
            .getItemCollection()
            .doc(item.id)
            .delete()
    }

    render() {
        return (
            <div>
                <List className='task-list'>
                    {this.props.items.map((item) =>
                        <ListItem key={item.id}
                            className='task-item-container'>
                            <ListItemIcon>
                                <Checkbox
                                    edge="start"
                                    color="primary"
                                    onClick={() => this.onDoneClick(item)}
                                />
                            </ListItemIcon>
                            <ListItemText
                                className={item.data.done ? 'item-text-done' : 'item-text'}
                                primary={item.data.title} />
                            <ListItemSecondaryAction>
                                <IconButton edge="end" aria-label="comments">
                                    <CloseIcon
                                        onClick={() => this.onDeleteClick(item)} />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>)}
                </List>
            </div>
        )
    }
}
