import React from 'react';
import TaskList from '../task-list';
import Input from '../input';

import Button from '@material-ui/core/Button';

import './account.css';


class Account extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: []
        }
    }

    componentDidMount() {
        let self = this;
        this.props.firebase.getItemCollection()
            .onSnapshot(function (querySnapshot) {
                var items = [];
                querySnapshot.forEach(function (doc) {
                    items.push({
                        data: doc.data(),
                        id: doc.id
                    });
                });
                self.setState({
                    items: items
                })
            })
    }

    onAddBtnClick = (inputText) => {
        this.props.firebase
            .getItemCollection()
            .add({
                title: inputText,
                done: false
            });
        this.setState({
            inputItem: ''
        })
    }

    render() {
        return (
            <div>
                <div className='sign-out-btn'>
                    <Button
                        variant="contained"
                        onClick={() => this.props.firebase.signOut()}
                    >
                        Sing Out
                </Button>
                </div>
                <h3>To do list</h3>
                <div className="account-container">
                    <Input onAddBtnClick={this.onAddBtnClick} />
                    <TaskList
                        items={this.state.items}
                        firebase={this.props.firebase} />
                </div>
            </div>
        )
    }

}

export default Account;
