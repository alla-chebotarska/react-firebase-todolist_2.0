import React from 'react';

import Button from '@material-ui/core/Button';

import './home.css'

class Home extends React.Component {

    render() {
        return (
            <div className='home-container'>
                <Button 
                variant="contained" 
                color="secondary" 
                onClick={() => this.props.firebase.signInWithGoogle()}>Log in with Google</Button>
            </div>
        )
    }

}

export default Home;