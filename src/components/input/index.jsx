import React from 'react'

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import './input.css';

export default class Input extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            inputItem: ''
        }
    }

    onInputChanged = (event) => {
        this.setState({
            inputItem: event.target.value
        })
    }

    onAddBtnClick = () => {
        this.props.onAddBtnClick(this.state.inputItem);
        this.setState({
            inputItem: ''
        })
    }

    render() {
        return (
            <div className='input-container'>
                <TextField
                    id="standard-basic"
                    label="To do"
                    className='add-task-input'
                    onInput={this.onInputChanged}
                    value={this.state.inputItem}
                />
                <Button
                    variant="contained"
                    color="primary"
                    className="add-task-btn"
                   onClick={this.onAddBtnClick}>
                    Add
                </Button>
            </div>
        )
    }

}
